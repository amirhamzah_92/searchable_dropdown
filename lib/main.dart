import 'package:flutter/material.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  title: "Flutter Dropdown Search",
  theme: ThemeData(
    brightness: Brightness.light,
  ),
  home: Home(),
));

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String selectedValue;
  final List<DropdownMenuItem> jobCodes = [];

  @override
  void initState() {

    jobCodes.add(DropdownMenuItem(
      child: Text('Petronas'),
      value: 'Petronas',
    ));
    jobCodes.add(DropdownMenuItem(
      child: Text('TNB'),
      value: 'TNB',
    ));
    jobCodes.add(DropdownMenuItem(
      child: Text('Chubb'),
      value: 'Chubb',
    ));
    jobCodes.add(DropdownMenuItem(
      child: Text('Newspage'),
      value: 'Newspage',
    ));
    jobCodes.add(DropdownMenuItem(
      child: Text('UOB'),
      value: 'UOB',
    ));
    jobCodes.add(DropdownMenuItem(
      child: Text('Cimb'),
      value: 'Cimb',
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Search Dropdown"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SearchableDropdown.single(
              items: jobCodes,
              value: selectedValue,
              hint: "Select Job Code",
              searchHint: "Select Job Code",
              onChanged: (value) {
                setState(() {
                  selectedValue = value;
                });
              },
              doneButton: "Done",
              displayItem: (item, selected) {
                return (Row(children: <Widget>[
                  selected
                      ? Icon(
                    Icons.radio_button_checked,
                    color: Colors.grey,
                  )
                      : Icon(
                    Icons.radio_button_unchecked,
                    color: Colors.grey,
                  ),
                  SizedBox(width: 7),
                  Expanded(
                    child: item,
                  ),
                ]));
              },
              isExpanded: true,
            )
          ],
        ),
      ),
    );
  }
}
